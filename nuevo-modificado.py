import numpy
import pyaudio
import math
# import gpiozero as gpio
# import inserta_db_josue as db
 
class ToneGenerator(object):
 
    def __init__(self, fs=44100, frames_per_buffer=4410):
        self.p = pyaudio.PyAudio()
        self.fs = fs
        self.frames_per_buffer = frames_per_buffer
        self.streamOpen = False
 
    def ondasenoidal(self):
        if self.buffer_offset + self.frames_per_buffer - 1 > self.x_max:
            # We don't need a full buffer or audio so pad the end with 0's
            xs = numpy.arange(self.buffer_offset,
                              self.x_max)
            tmp = self.amplitude * numpy.sin(xs * self.omega)
            out = numpy.append(tmp,
                               numpy.zeros(self.frames_per_buffer - len(tmp)))
        else:
            xs = numpy.arange(self.buffer_offset,
                              self.buffer_offset + self.frames_per_buffer)
            out = self.amplitude * numpy.sin(xs * self.omega)
        self.buffer_offset += self.frames_per_buffer
        return out
 
    def callback(self, in_data, frame_count, time_info, status):
        if self.buffer_offset < self.x_max:
            data = self.ondasenoidal().astype(numpy.float32)
            return (data.tostring(), pyaudio.paContinue)
        else:
            return (None, pyaudio.paComplete)
 
    def is_playing(self):
        if self.stream.is_active():
            return True
        else:
            if self.streamOpen:
                self.stream.stop_stream()
                self.stream.close()
                self.streamOpen = False
            return False
 
    def play(self, frequency, duration, amplitude):
        self.omega = float(frequency) * (math.pi * 2) / self.fs
        self.amplitude = amplitude
        self.buffer_offset = 0
        self.streamOpen = True
        self.x_max = math.ceil(self.fs * duration) - 1
        self.stream = self.p.open(format=pyaudio.paFloat32,
                                  channels=1,
                                  rate=self.fs,
                                  output=True,
                                  frames_per_buffer=self.frames_per_buffer,
                                  stream_callback=self.callback)
 
 
###############################################################################
#                                 Usage Example                               #
###############################################################################
 
generator = ToneGenerator()

# button = gpio.Button(17) #pin 11

frequency_start = 125        # Frequency to start the sweep from
frequency_end = 8000       # Frequency to end the sweep at
num_frequencies = 10       # Number of frequencies in the sweep
amplitude = 1            # Amplitude of the waveform
step_duration = 0.2        # Time (seconds) to play at each step
# step_duration = 1        # Time (seconds) to play at each step

name = input("Ingrese su nombre")
 
for frequency in numpy.logspace(math.log(frequency_start, 10),
                                math.log(frequency_end, 10),
                                num_frequencies):
 
    print("Reproduciendo a {0:0.2f} Hz".format(frequency))
    generator.play(frequency, step_duration, amplitude)

    if(button.is_pressed):
        db.InsertDatos(str(name),str(frequency),str(amplitude))
        print("presionado")
    while generator.is_playing():
        pass                # Do something useful in here (e.g. recording)