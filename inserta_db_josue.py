import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode
from datetime import datetime
def InsertDatos(d_nombre,d_frecuencia,d_Volumendb):
    try:
        connection = mysql.connector.connect(host='localhost',database='tsistemas',user='root',password='r@spberry')
        cursor = connection.cursor()
        sql_insert_query = """ INSERT INTO `datos` (`nombre`,`frecuencia`, `volumen`) VALUES (%s,%s,%s)"""
        insert_tuple = (d_nombre,d_frecuencia, d_Volumendb)
        result  = cursor.execute(sql_insert_query, insert_tuple)
        connection.commit()
        #print ("Registro insertado exitosamente en la tabla")
    except mysql.connector.Error as error :
        connection.rollback()
        print("Error al insertar en la tabla MySQL{}".format(error))
    finally:
        #closing database connection.
        if(connection.is_connected()):
            cursor.close()
            connection.close()
            #print("Conexión MySQL está cerrada")

##InsertDatos("frecuencia 1","volumen1")