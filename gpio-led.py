#enciende un led
import gpiozero as gpio
import time

led = gpio.LED(4) #GPI04 pin 7

while True:
        led.on()
        time.sleep(1)
        led.off()
        time.sleep(1)