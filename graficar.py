import numpy as np
import matplotlib.pyplot as matplotlib
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

connection = mysql.connector.connect(host='localhost',
                                     database='tsistemas',
                                     user='root',
                                     password='r@spberry')



cursor = connection.cursor()
cursor.execute("select frecuencia, amplitud from audiometrias where nombre like '%julio%' ORDER by frecuencia,amplitud")
result = cursor.fetchall()


frecuencia = []
amplitud = []

for row in result:
    frecuencia.append(float(row[0]))
    amplitud.append(float(row[1]))
    
connection.close()

matplotlib.plot(frecuencia,amplitud,label="Feedback usuario Audiometro")
matplotlib.show()

